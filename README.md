# Paper Keyboard
Would you like to try an ortholiear keyboard before you buy one?  
Would you like to try dvorak, but without the hassle of having to pull off all your keycaps?  
How about trying an *ortholiear dvorak keyboard*?  
Well with this *paper keyboard*, all of that is possible!  

This project utilizes your webcam, opencv, and tensorflow to let you use a drawing you made  
of your own-, custom keyboard layout.

---
### Table of contents  
- [Tensorflow model](#tensorflow-model)
- [Requirements](#requirements)
- [How to set up](#how-to-set-up)
- [How it works](#how-it-works--how-to-use)
- [Demo video](#demo-video)

---  
## Tensorflow model  
The provided tensorflow model was trained on ~400'000 images of uppercase letters from the english alphabet (A-Z).  
The dataset was pulled from NIST and modified a bit. The original can be found [here](https://www.nist.gov/srd/nist-special-database-19).  
As such, the program will currently only correctly detect keys of this type. Unfortunately no lowercase letters, or numbers.

You can of course always train your own model if you want, and replace the one in this repo.  
To ensure "drag-and-drop-compatibility", make sure that your model is trained on 64x64 pixel grayscale images,  
as that are the sort of images that the program will ask the model to classify.

---
## Requirements  
In order to run this project you need the following:
- Paper and pen
- A decent webcam (HD or better)
- Python
- OpenCV
- TensorFlow
- Computer with Linux (Nvidia Jetson Nano in my case)

---
## Preparation
- Install the Nvidia Jetson Nano image as described [here](https://developer.nvidia.com/embedded/learn/get-started-jetson-nano-devkit#write).  
- Once installed, run the following commands:  
`$ sudo apt update`  
`$ sudo apt upgrade`  
`$ sudo systemctl disable nvzramconfig`  
`$ sudo fallocate -l 4G /mnt/4GB.swap`  
`$ sudo chmod 600 /mnt/4GB.swap`  
`$ sudo mkswap /mnt/4GB.swap`  
`$ sudo su`  
`$ echo "/mnt/4GB.swap swap swap defaults 0 0" >> /etc/fstab`
`$ exit`  
`$ reboot`
- Follow [this](https://docs.nvidia.com/deeplearning/frameworks/install-tf-jetson-platform/index.html) guide to install tensorflow on your jetson nano.  
Note that on step 3, you need to replace "/v50" with "/v44".  
This process might take a while.
- Once done, install evdev with:  
`$ pip3 install evdev`
- To run the script, open a terminal inside the `src` folder and do:  
    `$ sudo chmod +x ./keyboard.py` (first time only)  
    `$ sudo ./keyboard.py`


---
## How it works / How to use 
Before you start, you have to:  

0. **Draw a keyboard on a piece of paper and point your camera at it :^)**  

    This can be any layout you want, so long as you only use uppercase A-Z (as mentioned [here](#tensorflow-model)).  
    Make sure the lines are fairly straight and thick, so they contrast well with the paper.  
    (A black sharpie is recommended, but certain dark colors might work as well)  
    The camera is configured such that it be put on a monitor like normal, and face down on the keyboard.  
    The program will flip the image by itself.  
    Example keyboard:  
    <img src="https://gitlab.com/LucasEmmes/noroff-final/-/raw/main/media/example_keyboard.jpg" width="250" height="187">

Once booted up, the program takes 3 steps to work.  
1. **Key detection (opencv)**  

    Once started, the program gives you a b/w feed of your webcam and asks you  
    to adjust it so that the keys are clearly visible. For this you use the slider at the top of the window.  

    Once you press "q" (on your real keyboard), opencv will try to detect where  
    your keys are and send this information to step 2.  

    Example:  
    <img src="https://gitlab.com/LucasEmmes/noroff-final/-/raw/main/media/example_detected.png" width="250" height="140">

2. **Character recognition (tensorflow)**  

    Once the program thinks it knows where your keys are, it will cut them out one by one  
    and attempt to read what letter you have written inside it.  
    Once every key has been assigned a character, it will visualize this by overlaying it onto  
    the webcam, and ask you how it looks.  

    If it looks good, you can press "y" to start using it in step 3, or "n" to go back to step 1 and  
    adjust the camera some more.  

    Example:  
    <img src="https://gitlab.com/LucasEmmes/noroff-final/-/raw/main/media/example_parsed.png" width="250" height="140">

3. **Detecting key presses (opencv)**

    If everything looks good, you can start using your keyboard!  
    The program will constantly check whether the keys it found earlier are still visible or not,  
    and register key presses based on this using the evdev library.  
    Simply hold your finger over the button you want to press and wait for it to register!  
    Note that there is some delay both due to the computational cost, and to avoid false positives.

---  
## Demo video  
Here is a [demo video](https://vimeo.com/765615029) showing what the keyboard might look like in action.
