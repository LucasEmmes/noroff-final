import numpy as np
import cv2

def equal(k1, k2):
    for i, v in enumerate(zip(k1, k2)):
        a, b = v
        if a[0] != b[0] or a[1] != b[1]: return False
    return True


def order_box(box):
    srt = np.argsort(box[:, 1])
    btm1 = box[srt[0]]
    btm2 = box[srt[1]]

    top1 = box[srt[2]]
    top2 = box[srt[3]]

    bc = btm1[0] < btm2[0]
    btm_l = btm1 if bc else btm2
    btm_r = btm2 if bc else btm1

    tc = top1[0] < top2[0]
    top_l = top1 if tc else top2
    top_r = top2 if tc else top1

    return np.array([top_l, top_r, btm_r, btm_l])

def contain_overlap(s1, s2):
    sq1 = order_box(s1)
    control_area = cv2.contourArea(s1)
    
    for point in s2:
        test_area = []
        for i in range(4):
            test_area.append(cv2.contourArea(np.array([sq1[i], sq1[(i+1)%4], point])))
        if abs(sum(test_area)-control_area) < 0.1: return True
    
    return False