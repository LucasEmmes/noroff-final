#! /usr/bin/python3

from key_functions import *
import time
from evdev import uinput, ecodes as e
import tensorflow as tf

keycodes = {"A":e.KEY_A, "B":e.KEY_B, "C":e.KEY_C, "D":e.KEY_D, "E":e.KEY_E, "F":e.KEY_F, "G":e.KEY_G, "H":e.KEY_H, "I":e.KEY_I, "J":e.KEY_J, "K":e.KEY_K, "L":e.KEY_L, "M":e.KEY_M, "N":e.KEY_N, "O":e.KEY_O, "P":e.KEY_P, "Q":e.KEY_Q, "R":e.KEY_R, "S":e.KEY_S, "T":e.KEY_T, "U":e.KEY_U, "V":e.KEY_V, "W":e.KEY_W, "X":e.KEY_X, "Y":e.KEY_Y, "Z":e.KEY_Z}

mask_threshold = 117
def adjust_threshold(v):
    """Adjusts the threshold of the mask\n
    Used in combination with the slider"""
    global mask_threshold
    mask_threshold = v

def get_frame(cap):
    """Returns the next frame from the camera and flips it"""
    _, frame = cap.read()
    frame = cv2.flip(frame, -1)
    return frame

def get_mask(frame, threshold, erode = True):
    """Returns a mask of the provided frame.
    Erosion recommended for contour-detection, but not key-parsing"""
    mask = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    _, mask = cv2.threshold(mask, threshold, 255, cv2.THRESH_BINARY)
    if erode: mask = cv2.erode(mask, None, iterations=1)
    return mask

def measure_key(key, mask):
    """Measures the amount of white pixels in the key-area of the mask"""
    # Get mask of where to cut out
    keymask = np.zeros(mask.shape, dtype='uint8')
    background = keymask.copy()
    cv2.drawContours(keymask, np.array([key]), -1, (255), -1)
    # Cut out
    background = cv2.bitwise_and(mask, mask, mask=keymask)
    return np.count_nonzero(background)


def parse_keys(keys, frame, model):
    """Takes a list of keys and uses AI to classify which letter is inside them.
    Returns a dictionairy which maps index of the key to the most likely letter"""
    print("Reading buttons")
    key_letters = {}
    for i, key in enumerate(keys):
        # Cut out key and do keystone correction on it
        button_size = 32
        src = np.array(order_box(key))
        dst = np.array([[0,1],[1,1],[1,0],[0,0]]) * button_size
        H = cv2.findHomography(src, dst, cv2.LMEDS)
        button_img = cv2.warpPerspective(frame.copy(), H[0], (button_size,button_size))
        button_img = get_mask(button_img, mask_threshold, False)
        button_img = cv2.bitwise_not(button_img)

        # Find and then remove the border around the key
        x = y = 0
        for k, row in enumerate(button_img):
            s = np.where(row == 255)[0]
            if len(s) > 0: 
                y = k
                x = s[0]
                break
        cv2.floodFill(button_img, None, (x, y), (0))

        # Classify the letter inside the key and store
        p = model.predict(np.array([button_img/255])).argmax()
        letter = chr(p + 65)
        key_letters[i] = letter
        print("Finished reading", i+1, "/", len(keys))
    return key_letters
    

def main():
    # Open camera
    cap = cv2.VideoCapture(0)
    if not cap.isOpened():
        print("Could not open camera")
        exit()
    print("Starting... This might take a while.")
    model = tf.keras.models.load_model("ocr_model_az.h5")
    model.predict(np.array([np.zeros((32,32))]))
    
    # Set up slider
    cv2.namedWindow("Mask")
    cv2.createTrackbar("Threshold", "Mask", 117, 255, adjust_threshold)
    
    # Loop to configure keyboard
    print("Please adjust slider until the outlines of the keyboard are clearly visible.")
    print("When you are satisfied, press 'q' to advance.")
    keys = []
    setup_done = False
    while True:

        # Make and show mask
        frame = get_frame(cap)
        mask = get_mask(frame, mask_threshold)
        cv2.imshow("Mask", mask)
       
        # Check for correctness
        if cv2.waitKey(1) == ord('q'):
            # Find keys and highlight them
            keys = get_all_keys(mask)
            keys = reduce_keys(mask.shape, keys)
            print("Detected", len(keys), "keys")
            highlighted_keys = frame.copy()
            cv2.drawContours(highlighted_keys, keys, -1, (255,0,0), -1)
            
            # Order and parse keys
            keys = np.array(sorted(keys, key=lambda x: x[0][1]))
            letters = parse_keys(keys, frame, model)

            # Visualise what the computer thinks you wrote on the keys
            for i, key in enumerate(keys):
                highlighted_keys = cv2.putText(highlighted_keys, letters[i], tuple(order_box(key)[0]), cv2.FONT_HERSHEY_SIMPLEX, fontScale=1.5, color=(0,0,0), thickness=2)

            # Check in with user
            cv2.imshow("Mask", highlighted_keys)
            print("Looks good? Press Y/N")
            
            while True:
                # If correct, continue to keyboard-mode
                if cv2.waitKey(1) == ord('y'):
                    setup_done = True
                    break
                # Otherwise, go back to adjust camera
                elif cv2.waitKey(1) == ord('n'):
                    break

        if setup_done: break
    cv2.destroyWindow("Mask")

    # Section for detecting key presses

    # Setup
    original_size = []
    last_seen = [0]*len(keys)
    for i, key in enumerate(keys):
        original_size.append(measure_key(key, mask))

    # Detection loop
    while True:
        frame = get_frame(cap)
        mask = get_mask(frame, mask_threshold, False)
        # Detect which key we are currently pointing at.
        # From the top of the screen and downwards, check all keys.
        # Once something is found, skip
        for i, key in enumerate(keys):
            current_size = measure_key(key, mask)
            if current_size < original_size[i]*0.7:
                if last_seen[i] == 0:
                    last_seen = [0]*len(keys)
                    last_seen[i] = 1
                elif last_seen[i] == 10:
                    # Send keyboard signal to OS
                    with uinput.UInput() as ui:
                        ui.write(e.EV_KEY, e.KEY_LEFTSHIFT, 1)
                        ui.write(e.EV_KEY, keycodes[letters[i]], 1)
                        ui.syn()
                    last_seen = [0]*len(keys)
                else:
                    last_seen[i] += 1
                break
            elif last_seen[i] > 0:
                last_seen[i] = 0


if __name__=="__main__":
    main()