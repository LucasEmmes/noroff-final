from geometry import *

def get_all_keys(mask):
    """Detects all the visible contours in a mask,\n
    and returns an array containing the ones it believes are keyboard keys"""
    keys = []
    contours = cv2.findContours(mask.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
    # Iterate over all the contours
    if len(contours) > 0:
        for i, cnt in enumerate(contours):
            if i == 0: continue
            # We only care about the ones which have 4 corners
            approx = cv2.approxPolyDP(cnt, 0.10  * cv2.arcLength(cnt, True), True)
            if len(approx) == 4:
                # Of the rectangular-ish contours we only want the ones which are "key-size"
                rect = cv2.minAreaRect(cnt)
                rect = np.int0(cv2.boxPoints(rect))
                keys.append(rect)

    return np.array(keys)

def get_key_contour_area(frame, key):
    # First, we get a cutout of the key in question
    key_mask = np.asarray(np.zeros(frame.shape[:2]), dtype="uint8")
    cv2.drawContours(key_mask, [key], -1, 255, -1)
    key_cutout = cv2.bitwise_and(frame, frame, mask=key_mask)

    # Second, we get the contour, and 
    gray_cutout = cv2.cvtColor(key_cutout, cv2.COLOR_BGR2GRAY)
    _, key_mask = cv2.threshold(gray_cutout, 127, 255, cv2.THRESH_BINARY)
    key_contours = cv2.findContours(key_mask.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
    return sum([cv2.contourArea(cnt) for cnt in key_contours])

def reduce_keys(shape, keys):
    """Takes in an array of rectanglular contours,\n
    and removes the ones which are contained within other rectngles"""
    not_contained = keys.copy().tolist()
    # Remove outliers so we only have keyboard keys to work with
    # Sort by size, and take an average of the middle 80%
    # Anything +- 100% of that average gets booted
    not_contained = sorted(not_contained, key=lambda x: cv2.contourArea(np.array(x)))
    l = len(not_contained)
    avg = sum([cv2.contourArea(np.array(k))  for k in not_contained[int(l//10):int(l//1.11)]]) / (l//1.25)
    not_contained = list(filter(lambda x: cv2.contourArea(np.array(x)) > avg*0.25 and cv2.contourArea(np.array(x)) < avg*4, not_contained))

    # I know that this isn't very (or at all) optimized,
    # but right now I just want to make this work
    for i, key in enumerate(keys):
        counter = 0
        while counter < len(not_contained):
            if contain_overlap(key, not_contained[counter]):
                if cv2.contourArea(np.array(key)) > cv2.contourArea(np.array(not_contained[counter])):
                    not_contained.pop(counter)
                else:
                    counter += 1
            else:
                counter += 1
    return np.array(not_contained) 